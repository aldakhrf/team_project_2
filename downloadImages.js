const http = require("http"); // or 'https' for https:// URLs
const fs = require("fs");

for (i = 0; i < 100; i++) {
    console.log(i + ' downloading image ..');
  const file = fs.createWriteStream(
    "public/assets/images/image_" + Math.floor(Math.random() * 9999999) + ".jpg"
  );
  const request = http.get(
    "http://placeimg.com/312/312/people",
    function (response) {
      response.pipe(file);
    }
  );
}
